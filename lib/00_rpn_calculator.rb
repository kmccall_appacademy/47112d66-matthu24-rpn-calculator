class RPNCalculator
  # TODO: your code goes here!

  def initialize
    #instance variable stack is array
    @stack = []
  end

  def push(num)
    @stack << num
  end

  def plus
      perform_operation(:+)
  end

  def minus
    perform_operation(:-)
  end

  def times
    perform_operation(:*)
  end

  def divide
    perform_operation(:/)
  end

  def value
    @stack.last
  end

  def tokens(string)
  #  tokenized = []
  #  string.split(" ").each do |el|
  #    if ["*","/","+","-"].include?(el)
  #      tokenized << el.to_sym
  #    else
  #      tokenized << el.to_i
  #    end
    #end
    #tokenized


    #use map
    tokenized = string.split(" ")
    tokenized.map {|token| ["*","/","+","-"].include?(token) ? token.to_sym : Integer(token)}

  end

  def evaluate(string)
    #make an array of operands and operators
    tokenized = tokens(string)
    tokenized.each do |token|
      if token.is_a? Integer
        push(token)
      elsif token.is_a? Symbol
        perform_operation(token)
      end
    end
    value
  end

  private

  def perform_operation(op_symbol)
    raise "calculator is empty" if @stack.size < 2
    second_operand = @stack.pop
    first_operand = @stack.pop
    if op_symbol == :+
      @stack << second_operand + first_operand
    elsif op_symbol == :-
      @stack << first_operand -second_operand
    elsif op_symbol == :*
      @stack << first_operand * second_operand
    elsif op_symbol == :/
      @stack << first_operand.fdiv(second_operand)
    end
  end
end
